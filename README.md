# Aws-serverless-api

serverless solution


## Diagram step 1

![AwsLambda.drawio](/uploads/31715b1cb53c2af4609e0099f6397daa/AwsLambda.drawio.png)

## Diagram step 2

![step-2.drawio](/uploads/fa7574a0d1fa383eb159c2c47bb9ed5d/step-2.drawio.png)

## Diagram step 3

![step3drawio](/uploads/695e76927f008552fceafe30e88d8342/step3drawio.png)

## Diagram step 4
![step4.drawio](/uploads/29a307d0f85a743629ad004345eac43e/step4.drawio.png)
