## PART 1 - SQS - Lambda - DynamoDB Table ##

Set region:
    Region: us-east-1

Note your AWS account number: *ACCOUNT NUMBER*

Create DDB Table:
	Name: ProductVisits
	Partition key: ProductVisitKey
	
Create SQS Queue:
	Name: ProductVisitsDataQueue
	Type: Standard